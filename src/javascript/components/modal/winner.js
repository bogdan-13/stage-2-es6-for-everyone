import { createFighterImage } from "../fighterPreview";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const title = `Winner is ${fighter.name}!`;
  const bodyElement = createFighterImage(fighter);
  const onClose = () => document.location.reload();
  showModal({ title, bodyElement, onClose });
}
