import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const pressedKeys = new Set();

    firstFighter.state = {
      blockKey: controls.PlayerOneBlock,
      possibleCritical: true,
      position: "left",
    };

    secondFighter.state = {
      blockKey: controls.PlayerTwoBlock,
      possibleCritical: true,
      position: "right",
    };

    makeFunctionWithKey(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, false, pressedKeys),
      [controls.PlayerOneAttack]
    );

    makeFunctionWithKey(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, false, pressedKeys),
      [controls.PlayerTwoAttack]
    );

    makeFunctionWithKey(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, true, pressedKeys),
      controls.PlayerOneCriticalHitCombination
    );

    makeFunctionWithKey(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, true, pressedKeys),
      controls.PlayerTwoCriticalHitCombination
    );
  });
}

const keyUpHandler = (event, pressedKeys) => {
  if (event.repeat) return;
 
  pressedKeys.delete(event.code);
};

const keyDownHandler = (event, func, codes, pressedKeys) => {
  if (event.repeat) return;

  pressedKeys.add(event.code);
  for (let code of codes) {
    if (!pressedKeys.has(code)) 
      return;
  }
  func();
};

function makeFunctionWithKey(pressedKeys, func, codes) {
  document.addEventListener("keydown", (e) =>
    keyDownHandler(e, func, codes, pressedKeys)
  );
  document.addEventListener("keyup", (e) => keyUpHandler(e, pressedKeys));
}

function fighterAttack(attacker, defender, resolve, isCrit, pressedKeys) {
  if (pressedKeys.has(defender.state.blockKey) && !isCrit) {
    return;
  }

  const possibleCritical = attacker.state.possibleCritical;
  const isAttackerBlocks = pressedKeys.has(attacker.state.blockKey);

  let damage = 0;
  if (isCrit && possibleCritical && !isAttackerBlocks) {
    setCriticalDelay(attacker);

    damage = getCriticalHitPower(attacker);
  }
  if (!isCrit && !isAttackerBlocks) {
    damage = getDamage(attacker, defender);
  }

  const isDefeated = reduceHealthBar(
    defender,
    damage,
    defender.state.position
  );

  if (isDefeated) {
    resolve(attacker);
  }
}

function setCriticalDelay(fighter) {
  fighter.state.canCrit = false;

  setTimeout(() => {
    fighter.state.canCrit = true;
  }, 10000);
}

function reduceHealthBar(fighter, damage, position) {
  const healthBar = document.getElementById(`${position}-fighter-indicator`);
  const healthBarWidth = healthBar.style.width || 100;
  const reducedHealthBar =
    parseFloat(healthBarWidth) - (damage / fighter.health) * 100;

  healthBar.style.width =
    reducedHealthBar > 0 ? `${reducedHealthBar}%` : 0;
  return reducedHealthBar < 0;
}



export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  return (Math.random() + 1) * attack;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  return (Math.random() + 1) * defense;
}

export function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}